<?php
function createDeepArrayOfNumbers($array, int $deep, int $x): array {
    $deep--;
    for ($i = 0; $i < $x; $i++) {
        if ($deep > 0) {
            $array[] = [];
            $array[$i] = createDeepArrayOfNumbers($array[$i], $deep, $x);
        }
        else {
            $array[] = random_int(10, 10000);
        }
    }
    return $array;
}

function calculateSum(array $deepArrayOfNumbers): int {
      return summ($deepArrayOfNumbers);
}


 function Summ(array $array):int{
    $sum=0;
    foreach ($array as $i){
        if (is_integer($i)){
            $sum +=$i;
        }
        else{
            $sum += Summ($i);
        }
    }
    return $sum;
 }
